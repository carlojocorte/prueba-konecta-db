/***Scripts Base de datos Prueba Konecta***/

/***Creación de la base de datos***/
CREATE DATABASE prueba_konecta_db;

/***Comienza la creación de las tablas***/
USE prueba_konecta_db;

/***Creación de tabla Empleado***/
CREATE TABLE empleado (
	id BIGINT(20) AUTO_INCREMENT PRIMARY KEY,
    fecha_ingreso DATE,
    nombre VARCHAR(50),
    salario INT
);

/***Creación de tabla Solicitud***/
CREATE TABLE Solicitud (
	id BIGINT(20) AUTO_INCREMENT PRIMARY KEY,
	codigo VARCHAR(50),
    descripcion VARCHAR(50),
    resumen VARCHAR(50),
    empleado_id BIGINT(20),
    FOREIGN KEY(id_empleado) 
       REFERENCES empleado(id)
);